package combate

import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import org.uqbar.commons.model.ObservableUtils

@Observable
class CombateAppModel {

	val generador = new GuerreroGenerator

	@Accessors
	Guerrero combatiente
	@Accessors
	Guerrero retador

	new() {
		nuevoCombate
	}

	def nuevoCombate() {
		val combatientes = generador.generarCombatientes
		combatiente = combatientes.get(0)
		retador = combatientes.get(1)
	}

	def reemplazarMuertos() {
		if(combatiente.estaMuerto) combatiente = generador.generarContrincanteDe(retador)
		if(retador.estaMuerto) retador = generador.generarContrincanteDe(combatiente)
	}

	def atacaRetador() {
		efectuarAtaque(retador, combatiente)
	}

	def atacaCombatiente() {
		efectuarAtaque(combatiente, retador)
	}

	def efectuarAtaque(Guerrero atacante, Guerrero defensor) {
		atacante.atacarA(defensor)
		ObservableUtils.firePropertyChanged(this, "algunoMuerto")
		ObservableUtils.firePropertyChanged(defensor, "estaVivo")
	}

	def getAlgunoMuerto() {
		combatiente.estaMuerto || retador.estaMuerto
	}
}
