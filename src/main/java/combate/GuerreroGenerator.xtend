package combate

import java.util.Random
import java.util.List

class GuerreroGenerator {

	val random = new Random()
	val nombres = #["Atila", "Genghis Khan", "William Wallace", "Arturo"]

	def randomEntre(Integer min, Integer max) {
		val difference = max - min
		random.nextInt(difference + 1) + min
	}

	def <T> T randomIn(List<T> elements) {
		elements.get(random.nextInt(elements.size))
	}

	def generar(String nombre) {
		new Guerrero(randomEntre(40, 60), randomEntre(15, 40), nombre)
	}

	def generarCombatientes() {
		nombres.sortBy[nombre|random.nextInt].take(2).map [
			generar(it)
		]
	}

	def generarContrincanteDe(Guerrero guerrero) {
		generar(randomIn(nombres.filter[it != guerrero.nombre].toList))
	}

}
