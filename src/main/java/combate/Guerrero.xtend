package combate

import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.UserException
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class Guerrero {

	Integer energia = 100
	val Integer potencialOfensivo
	val Integer potencialDefensivo
	val String nombre
	
	List<Guerrero> victimas = newArrayList()

	new(Integer potencialOfensivo, Integer potencialDefensivo, String nombre) {
		this.nombre = nombre
		this.potencialOfensivo = potencialOfensivo
		this.potencialDefensivo = potencialDefensivo
	}

	def atacarA(Guerrero otroGuerrero) {
		validarOponenteVivo(otroGuerrero)
		if (otroGuerrero.potencialDefensivo < this.potencialOfensivo) {
			otroGuerrero.recibirDanio(this.potencialOfensivo - otroGuerrero.potencialDefensivo)
			if(otroGuerrero.energia <= 0) {
				this.victimas.add(otroGuerrero)
			}
		}
	}
	
	def validarOponenteVivo(Guerrero otroGuerrero) {
		if(otroGuerrero.estaMuerto) {
			throw new UserException("Déjalo, ya está muerto!")
		}
	}
	
	def estaMuerto() {
		this.energia == 0
	}
	
	def getEstaVivo() {
		!estaMuerto
	}

	def recibirDanio(Integer danio) {
		this.energia = Math.max(this.energia - danio, 0)
	}

}
